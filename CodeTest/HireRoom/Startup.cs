﻿using Microsoft.Owin;
using NLog;
using Owin;
using SimpleInjector;
using System;
using System.Collections.Generic;
using Raven.Client;
using Raven.Client.Document;

[assembly: OwinStartup(typeof(HireRoom.Startup))]

namespace HireRoom
{
    public partial class Startup
    {
        private static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        private static Container _container;

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            Log.Info("Start App");

            Bootstrap();
        }

        private static void Bootstrap()
        {
            Container container = new Container();

            BatchRegister(container);

#if DEBUG
            try {
                container.Verify();
            }
            catch (Exception ex) {
                Log.Error(ex, "IoC Container Verify After Registration Error");
            }
#endif

            _container = container;

            IoC.GetInstance = GetInstance;
            IoC.GetAllInstances = GetAllInstance;
        }

        private static void BatchRegister(Container container)
        {
            container.RegisterSingle<IDocumentStore>(() => new DocumentStore { ConnectionStringName = "RavenCodeTest" });
            container.RegisterInitializer<IDocumentStore>(store => store.Initialize());
        }

        /// <summary>
        /// method to map with exist IoC Container libray
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static object GetInstance(Type serviceType, string key = null)
        {
            if (_container == null)
                throw new InvalidOperationException("IoC Container under Global.asax is null");
            return _container.GetInstance(serviceType);
        }

        /// <summary>
        /// method to map with exist IoC Container libray
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        private static IEnumerable<object> GetAllInstance(Type serviceType)
        {
            if (_container == null)
                throw new InvalidOperationException("IoC Container under Global.asax is null");
            return _container.GetAllInstances(serviceType);
        }
    }
}