﻿using HireRoom.Daos;
using HireRoom.Models.Dtos;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HireRoom.Controllers
{
    public class QuestionaireController : ApiController
    {
        [HttpGet, Route("questionaires/")]
        public async Task<QuestionaireDto[]> GetQuestionaires()
        {
            HireRoomDao dao = IoC.Get<HireRoomDao>();

            var questionaires = await dao.GetQuestionairesAsync();

            return
                questionaires.OrEmpty().Select(questionaire => questionaire.ToDto()).Where(dto => dto != null).ToArray();
        }

        [HttpGet, Route("questionaires/{hash}")]
        public async Task<QuestionaireDto> GetQuestionaire(string hash)
        {
            if (String.IsNullOrWhiteSpace(hash))
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "Invalid Hash"));
            HireRoomDao dao = IoC.Get<HireRoomDao>();

            var questionaire = await dao.GetQuestionaireAsync(hash);

            if (questionaire == null)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Questionaire does not exist"));

            return questionaire.ToDto();
        }
    }
}