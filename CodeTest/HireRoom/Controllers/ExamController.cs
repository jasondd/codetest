﻿using HireRoom.Daos;
using HireRoom.Models.Data;
using HireRoom.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HireRoom.Controllers
{
    public class ExamController : ApiController
    {
        /// <summary>
        /// Get exam for specific questionair with specific candidate's email. If exam does not exist, use add or update service to create empty exam.
        /// </summary>
        /// <param name="questionaireHash">questionair's hash</param>
        /// <param name="email">candidate's email identification</param>
        /// <returns></returns>
        [HttpGet, Route("exam/{questionaireHash}")]
        public async Task<ExamDto> GetExam(string questionaireHash, string email)
        {
            if (String.IsNullOrWhiteSpace(questionaireHash))
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "Invalid Hash"));
            if (String.IsNullOrWhiteSpace(email))
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "Invalid Email"));
            HireRoomDao dao = IoC.Get<HireRoomDao>();

            var exam = await dao.GetExamAsync(questionaireHash, email);

            if (exam == null)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Exam does not exist"));

            return exam.ToDto();
        }

        /// <summary>
        /// Add or update exam, this is used to update start and end time of exam. It also accept questions, if you prefer update all question answers all at once.
        /// </summary>
        /// <returns>Current exam after update</returns>
        [HttpPost, Route("exam/addorupdate")]
        public async Task<ExamDto> AddOrUpdateExam(AddOrUpdateExam req)
        {
            if (!ModelState.IsValid) {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
            }

            HireRoomDao dao = IoC.Get<HireRoomDao>();

            var exam = await dao.AddOrUpdateExamAsync(req);

            if (exam == null)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Exam does not exist"));

            return exam.ToDto();
        }

        /// <summary>
        /// This service is use to update exam's question individually. Which allow you to update question answer more frequent. For example, when
        /// user enter text field of question, we update that question's start time, then on leave we update end time and answer.
        /// </summary>
        [HttpPost, Route("exam/updateanswer/")]
        public async Task<ExamDto> UpdateQuestionAnswer(UpdateQuestionAnswer req)
        {
            if (!ModelState.IsValid) {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
            }

            HireRoomDao dao = IoC.Get<HireRoomDao>();

            var exam = await dao.UpdateQuestionAnswerAsync(req);

            if (exam == null)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Exam does not exist"));

            return exam.ToDto();
        }

        /// <summary>
        /// This is service to record specific question's snapshot. This is advanced function for hireroom. The goal is to take snapshot every specific interval. Where
        /// we can play through according to snapshot record. For example, if we record snapshot every 5 sec, then how candidate input its answer can be reproduced later
        /// </summary>
        [HttpPost, Route("exam/snapshots/add")]
        public async Task<QuestionSnapshotDto> AddSnapshot(AddSnapshot req)
        {
            if (!ModelState.IsValid) {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
            }

            HireRoomDao dao = IoC.Get<HireRoomDao>();

            var snapshot = await dao.AddSnapshotAsync(req);

            return snapshot.ToDto();
        }

        /// <summary>
        /// This is service to retrieve all snapshots for specific question. Where we can play through to see how candidate answer their question.
        /// </summary>
        /// <param name="examKey">exam key property</param>
        /// <param name="questionHash">question hash, each question under exam should have it's own hash.</param>
        /// <returns></returns>
        [HttpGet, Route("exam/snapshots/")]
        public async Task<QuestionSnapshotDto[]> GetSnapshots(string examKey, string questionHash)
        {
            if (String.IsNullOrWhiteSpace(examKey) || String.IsNullOrWhiteSpace(questionHash))
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            HireRoomDao dao = IoC.Get<HireRoomDao>();
            var snapshots = await dao.GetSnapshotsAsync(examKey, questionHash);

            return snapshots.OrEmpty().Select(snapshot => snapshot.ToDto()).ToArray();
        }

        /// <summary>
        /// This service is use to retrieve all exams. Questionair hash and candidate email are optional filter.
        /// </summary>
        [HttpGet, Route("exams")]
        public async Task<ExamDto[]> GetExams([FromUri]QueryExam req)
        {
            req = req ?? new QueryExam();

            HireRoomDao dao = IoC.Get<HireRoomDao>();
            ICollection<Exam> exams = await dao.GetExams(req.QuestionairHash, req.Email);

            return exams.Select(exam => exam.ToDto()).ToArray();
        }
    }
}