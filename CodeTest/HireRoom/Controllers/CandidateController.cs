﻿using HireRoom.Daos;
using HireRoom.Models.Data;
using HireRoom.Models.Dtos;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HireRoom.Controllers
{
    public class CandidateController : ApiController
    {
        [HttpGet, Route("candidates/")]
        public async Task<CandidateDto[]> GetCandidates()
        {
            HireRoomDao dao = IoC.Get<HireRoomDao>();
            var candidates = await dao.GetCandidatesAsync();

            return candidates.OrEmpty().Select(candidate => candidate.ToDto()).Where(dto => dto != null).ToArray();
        }

        [HttpGet, Route("candidates")]
        public async Task<CandidateDto> GetCandidate([FromUri]string email)
        {
            if (String.IsNullOrWhiteSpace(email))
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "Invalid Email"));

            HireRoomDao dao = IoC.Get<HireRoomDao>();
            var candidate = await dao.GetCandidateAsync(email);

            if (candidate == null)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    "Candidate does not exist"));

            return candidate.ToDto();
        }

        [HttpPut, HttpPost, Route("candidates/")]
        public async Task<CandidateDto> PutCandidate(AddCandidate req)
        {
            if (!ModelState.IsValid) {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
            }

            RegexEmailValidation validation = IoC.Get<RegexEmailValidation>();
            bool valid = validation.IsValidEmail(req.Email);
            if (!valid)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                    "Invalid Email"));

            HireRoomDao dao = IoC.Get<HireRoomDao>();
            var candidate = await dao.SaveCandidateAsync(new Candidate {
                Id = Candidate.CreateId(req.Email),
                Fullname = req.Fullname,
                Email = req.Email.Trim().ToLowerInvariant(),
                BirthDay = req.BirthDay
            });

            return candidate.ToDto();
        }
    }
}