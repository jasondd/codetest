﻿using HireRoom.Models.Data;
using HireRoom.Models.Dtos;
using Raven.Client;
using Raven.Client.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace HireRoom.Daos
{
    public class HireRoomDao
    {
        private readonly IDocumentStore _store;

        public HireRoomDao(IDocumentStore store)
        {
            _store = store;
        }

        public async Task<Candidate[]> GetCandidatesAsync()
        {
            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var candidates = await session.Advanced.DocumentStore.AsyncDatabaseCommands.StartsWithAsync(
                    Candidate.IdPrefix, null, 0, 100).ConfigureAwait(false);

                if (candidates.IsEmpty()) {
                    foreach (Candidate candidate in HireRoomDefault.Candidates) {
                        await session.StoreAsync(candidate).ConfigureAwait(false);
                    }
                    await session.SaveChangesAsync().ConfigureAwait(false);
                    candidates = await session.Advanced.DocumentStore.AsyncDatabaseCommands.StartsWithAsync(
                        Candidate.IdPrefix, null, 0, 100).ConfigureAwait(false);
                }

                return
                    candidates.OrEmpty()
                        .Select(
                            doc =>
                                ((InMemoryDocumentSessionOperations)session).TrackEntity<Candidate>(doc.Key,
                                    doc.DataAsJson, doc.Metadata, false))
                        .ToArray();
            }
        }

        public async Task<Candidate> SaveCandidateAsync(Candidate candidate)
        {
            if (candidate == null) {
                throw new ArgumentNullException("candidate");
            }
            if (String.IsNullOrWhiteSpace(candidate.Email)) {
                throw new ArgumentOutOfRangeException("candidate");
            }

            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                candidate.Id = Candidate.CreateId(candidate.Email);
                await session.StoreAsync(candidate).ConfigureAwait(false);
                await session.SaveChangesAsync();

                return candidate;
            }
        }

        public Task<Candidate> GetCandidateAsync(string email)
        {
            if (String.IsNullOrWhiteSpace(email)) {
                throw new ArgumentNullException("email");
            }

            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                string id = Candidate.CreateId(email);

                return session.LoadAsync<Candidate>(id);
            }
        }

        public async Task<Questionaire[]> GetQuestionairesAsync()
        {
            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var questionaires = await session.Advanced.DocumentStore.AsyncDatabaseCommands.StartsWithAsync(
                    Questionaire.IdPrefix, null, 0, 100).ConfigureAwait(false);

                if (questionaires.IsEmpty()) {
                    foreach (Questionaire questionaire in HireRoomDefault.Questionaires) {
                        await session.StoreAsync(questionaire).ConfigureAwait(false);
                    }
                    await session.SaveChangesAsync().ConfigureAwait(false);
                    questionaires = await session.Advanced.DocumentStore.AsyncDatabaseCommands.StartsWithAsync(
                        Questionaire.IdPrefix, null, 0, 100).ConfigureAwait(false);
                }

                return
                    questionaires.OrEmpty()
                        .Select(
                            doc =>
                                ((InMemoryDocumentSessionOperations)session).TrackEntity<Questionaire>(doc.Key,
                                    doc.DataAsJson, doc.Metadata, false))
                        .ToArray();
            }
        }

        public async Task<Questionaire> GetQuestionaireAsync(string hash)
        {
            if (String.IsNullOrWhiteSpace(hash))
                throw new ArgumentNullException("hash");

            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                string id = Questionaire.CreateId(hash);
                var questionaire = await session.LoadAsync<Questionaire>(id).ConfigureAwait(false);

                return questionaire;
            }
        }

        public async Task<Exam> AddOrUpdateExamAsync(AddOrUpdateExam req)
        {
            string questionaireId = Questionaire.CreateId(req.QuestionaireHash);
            string candidateId = Candidate.CreateId(req.Email);
            string examId = Exam.CreateId(req.QuestionaireHash, req.Email);

            Dictionary<string, QuestionAnswerDto> questions =
                req.Questions.OrEmpty().Where(q => q != null && q.Hash != null).ToDictionaryEx(q => q.Hash);

            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var exam = await session.LoadAsync<Exam>(examId).ConfigureAwait(false);
                if (exam == null) {
                    var questionaire =
                        await session.Include(candidateId).LoadAsync<Questionaire>(questionaireId).ConfigureAwait(false);
                    var candidate = await session.LoadAsync<Candidate>(candidateId).ConfigureAwait(false);
                    if (questionaire == null || candidate == null)
                        throw new HttpResponseException(HttpStatusCode.NotFound);
                    exam = questionaire.ToExam(candidate);
                    await session.StoreAsync(exam).ConfigureAwait(false);
                    await session.SaveChangesAsync().ConfigureAwait(false);
                }

                exam.StartTime = req.StartTime ?? exam.StartTime;
                exam.EndTime = req.EndTime ?? exam.EndTime;
                exam.UpdateQuestions(questions);

                await session.SaveChangesAsync().ConfigureAwait(false);

                return exam;
            }
        }

        public async Task<Exam> GetExamAsync(string hash, string email)
        {
            string examId = Exam.CreateId(hash, email);

            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var exam = await session.LoadAsync<Exam>(examId).ConfigureAwait(false);

                return exam;
            }
        }

        public async Task<Exam> UpdateQuestionAnswerAsync(UpdateQuestionAnswer req)
        {
            string examId = Exam.CreateId(req.ExamKey);
            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var exam = await session.LoadAsync<Exam>(examId).ConfigureAwait(false);

                if (exam == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                var answer = exam.Questions.FirstOrDefault(q => q.Hash == req.QuestionHash);

                if (answer == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                answer.AnswerText = req.AnswerText;
                answer.StartTime = req.StartTime ?? answer.StartTime;
                answer.EndTime = req.EndTime ?? answer.EndTime;

                await session.SaveChangesAsync().ConfigureAwait(false);

                return exam;
            }
        }

        public async Task<QuestionSnapshot> AddSnapshotAsync(AddSnapshot req)
        {
            string id = QuestionSnapshot.CreateId(req.ExamKey, req.QuestionHash, req.Time);
            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                QuestionSnapshot snapshot = new QuestionSnapshot {
                    Id = id,
                    ExamKey = req.ExamKey,
                    Hash = req.QuestionHash,
                    LogSnapshot = req.LogSnapshot,
                    AnswerText = req.AnswerText,
                    Time = req.Time,
                    QuestionText = req.QuestionText
                };

                await session.StoreAsync(snapshot).ConfigureAwait(false);
                await session.SaveChangesAsync().ConfigureAwait(false);

                return snapshot;
            }
        }

        public async Task<QuestionSnapshot[]> GetSnapshotsAsync(string examKey, string questionHash)
        {
            string prefix = QuestionSnapshot.CreatePrefix(examKey, questionHash);
            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var snapshots = await session.Advanced.DocumentStore.AsyncDatabaseCommands.StartsWithAsync(
                    prefix, null, 0, 500).ConfigureAwait(false);

                return snapshots.OrEmpty()
                    .Select(
                        doc =>
                            ((InMemoryDocumentSessionOperations)session).TrackEntity<QuestionSnapshot>(doc.Key,
                                doc.DataAsJson, doc.Metadata, false))
                    .OrderBy(snapshot => snapshot.Time).ToArray();
            }
        }

        public async Task<List<Exam>> GetExams(string questionairHash, string email)
        {
            List<Exam> exams = new List<Exam>();
            IAsyncDocumentSession session = _store.OpenAsyncSession();
            using (session) {
                var stream = await session.Advanced.StreamAsync<Exam>(Exam.IdPrefix);
                using (stream) {
                    while (await stream.MoveNextAsync()) {
                        Exam exam = stream.Current.Document;

                        if (!Take(exam, questionairHash, email))
                            continue;
                        exams.Add(exam);
                    }

                    return exams;
                }
            }
        }

        private static bool Take(Exam exam, string questionairHash, string email)
        {
            if (exam == null)
                return false;

            bool checkQuestionair = !String.IsNullOrEmpty(questionairHash);
            bool checkEmail = !String.IsNullOrEmpty(email);

            if (!checkQuestionair && !checkEmail)
                return true;

            if (checkQuestionair && (exam.QuestionaireId == null || !exam.QuestionaireId.EndsWith(questionairHash)))
                return false;

            if (!checkEmail)
                return true;

            return exam.Candidate != null && exam.Candidate.Email != null &&
                   exam.Candidate.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}