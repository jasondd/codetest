﻿using HireRoom.Models.Data;
using System;

namespace HireRoom.Daos
{
    public static class HireRoomDefault
    {
        public static readonly Candidate[] Candidates = {
            new Candidate {
                Id = Candidate.CreateId("john@hireroom.co.nz"),
                Fullname = "John Smith",
                Email = "john@hireroom.co.nz",
                BirthDay = DateTimeOffset.UtcNow.AddYears(-30)
            },
            new Candidate {
                Id = Candidate.CreateId("jan@hireroom.co.nz"),
                Fullname = "Jan Able",
                Email = "jan@hireroom.co.nz",
                BirthDay = DateTimeOffset.UtcNow.AddYears(-24)
            },
            new Candidate {
                Id = Candidate.CreateId("ayende@hireroom.co.nz"),
                Fullname = "Ayende",
                Email = "ayende@hireroom.co.nz",
                BirthDay = DateTimeOffset.UtcNow.AddYears(-40)
            },
                      new Candidate {
                Id = Candidate.CreateId("Benteke@hireroom.co.nz"),
                Fullname = "Christain Benteke",
                Email = "Benteke@hireroom.co.nz",
                BirthDay = DateTimeOffset.UtcNow.AddYears(-26)
            }
        };

        public static readonly Questionaire[] Questionaires = {
            new Questionaire {
                Id = Questionaire.CreateId("Da02"),
                Name = "Software Development",
                Hash = "Da02",
                Questions = new[] {
                    new Question {
                        Hash = "dd243",
                        QuestionText = "If we didn't have your university in our list then tell us about it here. Even if we did have it, tell us why you chose the university that you did.",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "cc244",
                        QuestionText = "What's different between java and javascript",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "ff345",
                        QuestionText = "Please write down method to calculate area of circle",
                        LogSnapshot = true
                    }
                    ,
                    new Question {
                        Hash = "ff346",
                        QuestionText = "What is the difference between a class and an object?",
                        LogSnapshot = true
                    },
                    new Question {
                        Hash = "ff347",
                        QuestionText = "Has anyone - friends, family, business or organisation - ever used your software?  Describe.",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "ff348",
                        QuestionText = "Do you have any Stackoverflow rep?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "ff349",
                        QuestionText = "What's the difference between HTTP & HTML?",
                        LogSnapshot = true
                    },
                    new Question {
                        Hash = "ff350",
                        QuestionText = "Can you run Java and Javascript on the same web page? Explain how?",
                        LogSnapshot = true
                    },
                    new Question {
                        Hash = "ff351",
                        QuestionText = "",
                        LogSnapshot = true
                    },
                    new Question {
                        Hash = "ff352",
                        QuestionText = "What is a responsive web site?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "ff353",
                        QuestionText = "What would you like to ask us or anything else we should know?",
                        LogSnapshot = false
                    }
                }
            },
            new Questionaire {
                Id = Questionaire.CreateId("coffee03"),
                Name = "Coffee Knowledge",
                Hash = "coffee03",
                Questions = new[] {
                  new Question {
                        Hash = "098ms",
                        QuestionText = "What colour is most coffee?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "010ms",
                        QuestionText = "Where does coffee come from?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "011ms",
                        QuestionText = "Does Tea or coffee contain more caffeine. Explain?",
                        LogSnapshot = false
                    },                  new Question {
                        Hash = "012ms",
                        QuestionText = "What is expresso?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "013ms",
                        QuestionText = "What is optimal coffee prep for 3 hour exam?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "014ms",
                        QuestionText = "Do you have any gunpla?",
                        LogSnapshot = false
                    },
                    new Question {
                        Hash = "origin",
                        QuestionText = "Have you watched Gundam Origin?",
                        LogSnapshot = true
                    }
                }
            }
        };
    }
}