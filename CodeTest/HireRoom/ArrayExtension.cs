﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HireRoom
{
    public static class ArrayExtension
    {
        public static IEnumerable<TValue> OrEmpty<TValue>(this IEnumerable<TValue> values)
        {
            return values ?? Enumerable.Empty<TValue>();
        }

        public static ICollection<TValue> OrEmptyCast<TValue>(this ICollection<TValue> values)
        {
            return values ?? new TValue[0];
        }

        public static IList<TValue> OrEmptyCast<TValue>(this IList<TValue> values)
        {
            return values ?? new TValue[0];
        }

        public static TValue[] OrEmptyCast<TValue>(this TValue[] values)
        {
            return values ?? new TValue[0];
        }

        public static List<TValue> OrEmptyList<TValue>(this List<TValue> values)
        {
            return values ?? new List<TValue>();
        }

        public static bool IsEmpty<TValue>(this IEnumerable<TValue> values)
        {
            if (values == null)
                return true;
            ICollection<TValue> collectionoft = values as ICollection<TValue>;
            if (collectionoft != null) return collectionoft.Count == 0;
            ICollection collection = values as ICollection;
            if (collection != null) return collection.Count == 0;

            return values.Any() == false;
        }

        public static IEnumerable<TValue> ExceptValues<TValue>(this IEnumerable<TValue> source,
            IEnumerable<TValue> target,
            IEqualityComparer<TValue> compare = null)
        {
            source = source.OrEmpty();
            target = target.OrEmpty();

            IEnumerable<TValue> result = compare == null
                ? source.Except(target).Distinct()
                : source.Except(target, compare).Distinct(compare);

            return result;
        }

        public static IEnumerable<TValue> UnionValues<TValue>(this IEnumerable<TValue> source,
            IEnumerable<TValue> target, IEqualityComparer<TValue> compare = null)
        {
            source = source.OrEmpty();
            target = target.OrEmpty();

            IEnumerable<TValue> result = compare == null
                ? source.Union(target).Distinct()
                : source.Union(target, compare).Distinct();

            return result;
        }

        /// <summary>
        ///     Creates a System.Collections.Generic.Dictionary[TKey,TValue] from an System.Collections.Generic.IEnumerable[T]
        ///     according to a specified key selector function, a comparer, and an element selector function.
        ///     Ex Version is to ignore if key already exist. In many case it is easier for database related process where we know
        ///     if key is same value should be same.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
        /// <param name="source">
        ///     An System.Collections.Generic.IEnumerable[T] to create a
        ///     System.Collections.Generic.Dictionary[TKey,TValue] from.
        /// </param>
        /// <param name="keySelector">A function to extract a key from each element.</param>
        /// <returns>
        ///     A System.Collections.Generic.Dictionary[TKey,TValue] that contains values of type TElement selected from the
        ///     input sequence.
        /// </returns>
        public static Dictionary<TKey, TSource> ToDictionaryEx<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector)
        {
            return ToDictionaryEx<TSource, TKey, TSource>(source, keySelector, x => x, null);
        }

        /// <summary>
        ///     Creates a System.Collections.Generic.Dictionary[TKey,TValue] from an System.Collections.Generic.IEnumerable[T]
        ///     according to a specified key selector function, a comparer, and an element selector function.
        ///     Ex Version is to ignore if key already exist. In many case it is easier for database related process where we know
        ///     if key is same value should be same.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
        /// <param name="source">
        ///     An System.Collections.Generic.IEnumerable[T] to create a
        ///     System.Collections.Generic.Dictionary[TKey,TValue] from.
        /// </param>
        /// <param name="keySelector">A function to extract a key from each element.</param>
        /// <param name="comparer">An System.Collections.Generic.IEqualityComparer[T] to compare keys.</param>
        /// <returns>
        ///     A System.Collections.Generic.Dictionary[TKey,TValue] that contains values of type TElement selected from the
        ///     input sequence.
        /// </returns>
        public static Dictionary<TKey, TSource> ToDictionaryEx<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            return ToDictionaryEx<TSource, TKey, TSource>(source, keySelector, x => x, comparer);
        }

        /// <summary>
        ///     Creates a System.Collections.Generic.Dictionary[TKey,TValue] from an System.Collections.Generic.IEnumerable[T]
        ///     according to a specified key selector function, a comparer, and an element selector function.
        ///     Ex Version is to ignore if key already exist. In many case it is easier for database related process where we know
        ///     if key is same value should be same.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
        /// <typeparam name="TElement">The type of the value returned by elementSelector.</typeparam>
        /// <param name="source">
        ///     An System.Collections.Generic.IEnumerable[T] to create a
        ///     System.Collections.Generic.Dictionary[TKey,TValue] from.
        /// </param>
        /// <param name="keySelector">A function to extract a key from each element.</param>
        /// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
        /// <returns>
        ///     A System.Collections.Generic.Dictionary[TKey,TValue] that contains values of type TElement selected from the
        ///     input sequence.
        /// </returns>
        public static Dictionary<TKey, TElement> ToDictionaryEx<TSource, TKey, TElement>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            return ToDictionaryEx<TSource, TKey, TElement>(source, keySelector, elementSelector, null);
        }

        /// <summary>
        ///     Creates a System.Collections.Generic.Dictionary[TKey,TValue] from an System.Collections.Generic.IEnumerable[T]
        ///     according to a specified key selector function, a comparer, and an element selector function.
        ///     Ex Version is to ignore if key already exist. In many case it is easier for database related process where we know
        ///     if key is same value should be same.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <typeparam name="TKey">The type of the key returned by keySelector.</typeparam>
        /// <typeparam name="TElement">The type of the value returned by elementSelector.</typeparam>
        /// <param name="source">
        ///     An System.Collections.Generic.IEnumerable[T] to create a
        ///     System.Collections.Generic.Dictionary[TKey,TValue] from.
        /// </param>
        /// <param name="keySelector">A function to extract a key from each element.</param>
        /// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
        /// <param name="comparer">An System.Collections.Generic.IEqualityComparer[T] to compare keys.</param>
        /// <returns>
        ///     A System.Collections.Generic.Dictionary[TKey,TValue] that contains values of type TElement selected from the
        ///     input sequence.
        /// </returns>
        public static Dictionary<TKey, TElement> ToDictionaryEx<TSource, TKey, TElement>(
            this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector,
            IEqualityComparer<TKey> comparer)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            if (keySelector == null)
                throw new ArgumentNullException("keySelector");
            if (elementSelector == null)
                throw new ArgumentNullException("elementSelector");
            Dictionary<TKey, TElement> dictionary = new Dictionary<TKey, TElement>(comparer);
            foreach (TSource source1 in source) {
                TKey key = keySelector(source1);
                // ReSharper disable once CompareNonConstrainedGenericWithNull
                if (key == null)
                    continue;
                dictionary[key] = elementSelector(source1);
            }
            return dictionary;
        }

        public static HashSet<TKey> ToHashSet<TKey>(this IEnumerable<TKey> source)
        {
            return source.ToHashSet(x => x, null);
        }

        public static HashSet<TKey> ToHashSet<TKey>(this IEnumerable<TKey> source, IEqualityComparer<TKey> comparer)
        {
            return source.ToHashSet(x => x, comparer);
        }

        public static HashSet<TKey> ToHashSet<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector)
        {
            return source.ToHashSet(keySelector, null);
        }

        public static HashSet<TKey> ToHashSet<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            if (keySelector == null)
                throw new ArgumentNullException("keySelector");
            HashSet<TKey> hashset = new HashSet<TKey>(comparer);
            foreach (TKey key in source.Select(keySelector).Where(key => key != null)) {
                hashset.Add(key);
            }
            return hashset;
        }

        public static TElement TryGet<TKey, TElement>(this IDictionary<TKey, TElement> dic, TKey key)
        {
            if (dic.IsEmpty() || key == null)
                return default(TElement);

            TElement res;
            dic.TryGetValue(key, out res);
            return res;
        }

        public static TElement TryGet<TKey, TElement>(this IDictionary<TKey, TElement> dic, TKey key, TElement def)
        {
            if (dic.IsEmpty() || key == null)
                return def;

            TElement res;
            return dic.TryGetValue(key, out res) ? res : def;
        }

        public static bool ContainsEx<TValue>(this HashSet<TValue> set, TValue value)
        {
            if (set.IsEmpty())
                return false;

            return value != null && set.Contains(value);
        }
    }
}