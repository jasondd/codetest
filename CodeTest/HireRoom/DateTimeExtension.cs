﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HireRoom
{
    public static class DateTimeExtension
    {
        public static Int64 ToUnixTimestampInSec(this DateTimeOffset time)
        {
            TimeSpan span = (time.UtcDateTime - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc));
            double unixTime = span.TotalSeconds;

            return Convert.ToInt64(unixTime);
        }
    }
}