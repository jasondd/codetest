﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace HireRoom
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new IsoDateTimeConverter {
                DateTimeFormat = "yyyy-MM-ddTHH:mm:ssZ",
                DateTimeStyles = DateTimeStyles.AdjustToUniversal
            });
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }

    /// <remarks>
    /// ServiceStack cors feature: https://github.com/ServiceStack/ServiceStack/blob/master/src/ServiceStack/CorsFeature.cs
    /// Mozilla cors explain: https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
    /// </remarks>>
    public class CorsFeatureMessageHandler : DelegatingHandler
    {
        internal const string DefaultMethods = "GET, POST, PUT, DELETE, OPTIONS";
        internal const string DefaultHeaders = "Content-Type";

        private readonly string _allowedMethods;
        private readonly string _allowedHeaders;
        private readonly string _exposeHeaders;
        private readonly bool _allowCredentials;

        public CorsFeatureMessageHandler(string allowedMethods = DefaultMethods,
            string allowedHeaders = DefaultHeaders, bool allowCredentials = false,
            string exposeHeaders = null)
        {
            _allowedMethods = allowedMethods;
            _allowedHeaders = allowedHeaders;
            _allowCredentials = allowCredentials;
            _exposeHeaders = exposeHeaders;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Method == HttpMethod.Options) {
                var response = GetOptionsResponse(request);
                return response;
            }

            var res = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);

            return res != null ? GetCorsResponse(res) : null;
        }

        private HttpResponseMessage GetOptionsResponse(HttpRequestMessage request)
        {
            var response = request.CreateResponse(HttpStatusCode.OK);
            return GetCorsResponse(response);
        }

        private HttpResponseMessage GetCorsResponse(HttpResponseMessage response)
        {
            AddAllowOrigin(response);
            if (!String.IsNullOrEmpty(_allowedMethods))
                AddHeaderIfEmpty(response, "Access-Control-Allow-Methods", _allowedMethods);
            if (_allowCredentials)
                AddHeaderIfEmpty(response, "Access-Control-Allow-Credentials", "true");
            if (!String.IsNullOrEmpty(_allowedHeaders))
                AddHeaderIfEmpty(response, "Access-Control-Allow-Headers", _allowedHeaders);
            if (!String.IsNullOrEmpty(_exposeHeaders))
                AddHeaderIfEmpty(response, "Access-Control-Expose-Headers", _exposeHeaders);

            return response;
        }

        private void AddAllowOrigin(HttpResponseMessage response)
        {
            if (response.RequestMessage == null)
                return;

            IEnumerable<string> origin;
            response.RequestMessage.Headers.TryGetValues("Origin", out origin);
            if (origin == null || _allowCredentials == false)
                AddHeaderIfEmpty(response, "Access-Control-Allow-Origin", "*");
            else
                AddHeaderIfEmpty(response, "Access-Control-Allow-Origin", origin);
        }

        private static void AddHeaderIfEmpty(HttpResponseMessage response, string header, string value)
        {
            if (String.IsNullOrEmpty(value))
                return;

            IEnumerable<string> exist;
            response.Headers.TryGetValues(header, out exist);
            if (exist == null)
                response.Headers.TryAddWithoutValidation(header, value);
        }

        private static void AddHeaderIfEmpty(HttpResponseMessage response, string header, IEnumerable<string> value)
        {
            if (value == null)
                return;

            IEnumerable<string> exist;
            response.Headers.TryGetValues(header, out exist);
            if (exist == null)
                response.Headers.TryAddWithoutValidation(header, value);
        }
    }
}