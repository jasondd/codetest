﻿using HireRoom.Models.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace HireRoom.Models.Dtos
{
    public class ExamDto
    {
        public string Key { get; set; }

        public CandidateDto Candidate { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }

        public QuestionAnswer[] Questions { get; set; }
    }

    public class AddOrUpdateExam
    {
        /// <summary>
        /// Hash value from questionair
        /// </summary>
        [Required]
        public string QuestionaireHash { get; set; }

        /// <summary>
        /// Email from specific candidate. Each candidate will have one unique exam for each questionair.
        /// </summary>
        [Required]
        public string Email { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }

        /// <summary>
        /// This is optional, you don't have to populate this when update exam.
        /// </summary>
        public QuestionAnswerDto[] Questions { get; set; }
    }

    public class QueryExam
    {
        public string QuestionairHash { get; set; }

        public string Email { get; set; }
    }

    public class QuestionAnswerDto
    {
        public string Hash { get; set; }

        public string AnswerText { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }
    }

    public class UpdateQuestionAnswer
    {
        /// <summary>
        /// Exam's key,
        /// </summary>
        [Required]
        public string ExamKey { get; set; }

        /// <summary>
        /// Question hash, which each question under exam should have it's own hash.
        /// </summary>
        [Required]
        public string QuestionHash { get; set; }

        public string AnswerText { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }
    }

    public static class ExamExtension
    {
        public static ExamDto ToDto(this Exam exam)
        {
            if (exam == null)
                return null;

            return new ExamDto {
                Candidate = exam.Candidate.ToDto(),
                StartTime = exam.StartTime,
                EndTime = exam.EndTime,
                Key = exam.Key,
                Questions = exam.Questions
            };
        }

        public static Exam ToExam(this Questionaire questionaire, Candidate candidate)
        {
            return new Exam {
                Id = Exam.CreateId(questionaire.Hash, candidate.Email),
                Candidate = candidate,
                Key = Exam.CreateKey(questionaire.Hash, candidate.Email),
                QuestionaireId = questionaire.Id,
                Questions = questionaire.Questions.Select(q => q.ToAnswer()).Where(q => q != null).ToArray()
            };
        }

        public static QuestionAnswer ToAnswer(this Question question)
        {
            if (question == null)
                return null;
            return new QuestionAnswer {
                Hash = question.Hash,
                QuestionText = question.QuestionText,
                LogSnapshot = question.LogSnapshot
            };
        }

        public static void UpdateQuestions(this Exam exam, Dictionary<string, QuestionAnswerDto> questions)
        {
            if (exam == null)
                throw new ArgumentNullException("exam");

            if (questions.IsEmpty())
                return;

            foreach (var question in exam.Questions) {
                var dto = questions.TryGet(question.Hash);
                if (dto == null)
                    continue;

                question.AnswerText = dto.AnswerText;
                question.StartTime = dto.StartTime ?? question.StartTime;
                question.EndTime = dto.EndTime ?? question.EndTime;
            }
        }
    }
}