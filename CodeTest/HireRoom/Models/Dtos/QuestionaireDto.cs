﻿using HireRoom.Models.Data;

namespace HireRoom.Models.Dtos
{
    public class QuestionaireDto
    {
        public string Name { get; set; }

        public string Hash { get; set; }

        public Question[] Questions { get; set; }
    }

    public static class QuestionaireExtension
    {
        public static QuestionaireDto ToDto(this Questionaire questionaire)
        {
            if (questionaire == null)
                return null;

            return new QuestionaireDto {
                Hash = questionaire.Hash,
                Name = questionaire.Name,
                Questions = questionaire.Questions
            };
        }
    }
}