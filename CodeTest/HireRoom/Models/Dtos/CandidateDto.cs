﻿using HireRoom.Models.Data;
using System;

namespace HireRoom.Models.Dtos
{
    public class CandidateDto
    {
        public string Fullname { get; set; }

        public string Email { get; set; }

        public DateTimeOffset? BirthDay { get; set; }
    }

    public static class CandidateExtension
    {
        public static CandidateDto ToDto(this Candidate candidate)
        {
            if (candidate == null)
                return null;
            return new CandidateDto {
                Email = candidate.Email,
                Fullname = candidate.Fullname,
                BirthDay = candidate.BirthDay
            };
        }
    }
}