﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HireRoom.Models.Dtos
{
    public class AddCandidate
    {
        [Required]
        public string Fullname { get; set; }

        [Required]
        public string Email { get; set; }

        public DateTimeOffset? BirthDay { get; set; }
    }
}