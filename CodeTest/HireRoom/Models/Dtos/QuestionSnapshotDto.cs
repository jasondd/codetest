﻿using HireRoom.Models.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace HireRoom.Models.Dtos
{
    public class QuestionSnapshotDto
    {
        public string ExamKey { get; set; }

        public string Hash { get; set; }

        public string QuestionText { get; set; }

        public bool LogSnapshot { get; set; }

        public string AnswerText { get; set; }

        public DateTimeOffset Time { get; set; }
    }

    public class AddSnapshot
    {
        /// <summary>
        /// Exam's key property
        /// </summary>
        [Required]
        public string ExamKey { get; set; }

        /// <summary>
        /// Question's hash property, each question under exam should have its own hash.
        /// </summary>
        [Required]
        public string QuestionHash { get; set; }

        public string QuestionText { get; set; }

        public bool LogSnapshot { get; set; }

        public string AnswerText { get; set; }

        public DateTimeOffset Time { get; set; }
    }

    public static class QuestionSnapshotExtension
    {
        public static QuestionSnapshotDto ToDto(this QuestionSnapshot question)
        {
            if (question == null)
                return null;

            return new QuestionSnapshotDto {
                ExamKey = question.ExamKey,
                Hash = question.Hash,
                QuestionText = question.QuestionText,
                LogSnapshot = question.LogSnapshot,
                AnswerText = question.AnswerText,
                Time = question.Time
            };
        }
    }
}