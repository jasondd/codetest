﻿using System;

namespace HireRoom.Models.Data
{
    public class QuestionSnapshot : IStrId, IQuestion
    {
        public const string IdPrefix = "QuestionSnapshot/";

        public static string CreateId(string examKey, string questionHash, DateTimeOffset time)
        {
            if (String.IsNullOrWhiteSpace(examKey))
                throw new ArgumentNullException("examKey");
            if (String.IsNullOrWhiteSpace(questionHash))
                throw new ArgumentNullException("questionHash");
            if (time < DateTimeOffset.UtcNow.AddDays(-1))
                throw new ArgumentOutOfRangeException("time");

            return IdPrefix +
                   String.Join("/", examKey.Trim(), questionHash.Trim(), time.ToUnixTimestampInSec());
        }

        public static string CreatePrefix(string examKey, string questionHash)
        {
            if (String.IsNullOrWhiteSpace(examKey))
                throw new ArgumentNullException("examKey");
            if (String.IsNullOrWhiteSpace(questionHash))
                throw new ArgumentNullException("questionHash");

            return IdPrefix +
                   String.Join("/", examKey.Trim(), questionHash.Trim()) + "/";
        }

        public string Id { get; set; }

        public string ExamKey { get; set; }

        public string Hash { get; set; }

        public string QuestionText { get; set; }

        public bool LogSnapshot { get; set; }

        public string AnswerText { get; set; }

        public DateTimeOffset Time { get; set; }
    }
}