﻿using System;

namespace HireRoom.Models.Data
{
    public class Candidate : IStrId
    {
        public const string IdPrefix = "Candidate/";

        public static string CreateId(string email)
        {
            if (String.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("email");

            return IdPrefix + email.Trim().ToLowerInvariant();
        }

        public string Id { get; set; }

        public string Fullname { get; set; }

        public string Email { get; set; }

        public DateTimeOffset? BirthDay { get; set; }
    }
}