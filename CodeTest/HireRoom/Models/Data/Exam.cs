﻿using System;

namespace HireRoom.Models.Data
{
    public class Exam : IStrId
    {
        public const string IdPrefix = "Exam/";

        public static string CreateId(string hash, string email)
        {
            string key = CreateKey(hash, email);
            return CreateId(key);
        }

        public static string CreateKey(string hash, string email)
        {
            if (String.IsNullOrWhiteSpace(hash))
                throw new ArgumentNullException("hash");
            if (String.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("email");

            hash = hash.Trim();
            email = email.Trim().ToLowerInvariant();

            string key = String.Join("/", hash, email);
            return key;
        }

        public static string CreateId(string key)
        {
            if (String.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            return IdPrefix + key.Trim();
        }

        public string Id { get; set; }

        public string Key { get; set; }

        public Candidate Candidate { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }

        public string QuestionaireId { get; set; }

        public QuestionAnswer[] Questions { get; set; }
    }

    public class QuestionAnswer : IQuestion
    {
        public string Hash { get; set; }

        public string QuestionText { get; set; }

        public bool LogSnapshot { get; set; }

        public string AnswerText { get; set; }

        public DateTimeOffset? StartTime { get; set; }

        public DateTimeOffset? EndTime { get; set; }
    }
}