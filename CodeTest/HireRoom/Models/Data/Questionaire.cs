﻿using System;

namespace HireRoom.Models.Data
{
    public class Questionaire : IStrId
    {
        public const string IdPrefix = "Questionaire/";

        public static string CreateId(string hash)
        {
            if (String.IsNullOrWhiteSpace(hash))
                throw new ArgumentNullException("hash");

            return IdPrefix + hash.Trim();
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Hash { get; set; }

        public Question[] Questions { get; set; }
    }

    public class Question : IQuestion
    {
        public string Hash { get; set; }

        public string QuestionText { get; set; }

        public bool LogSnapshot { get; set; }
    }

    public interface IQuestion
    {
        string Hash { get; set; }

        string QuestionText { get; set; }

        bool LogSnapshot { get; set; }
    }
}