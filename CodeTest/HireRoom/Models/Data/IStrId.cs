﻿namespace HireRoom.Models.Data
{
    public interface IStrId
    {
        string Id { get; set; }
    }
}